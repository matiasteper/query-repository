/*
Diego Voulminot  17:04
Estimados,
Les dejo esto por si les es de ayuda:
Saber el email, IP, Tarjeta usada en los Link de Pagos.
Si tienen manera mas facil de llegar a esta data favor compartirla!
Sino, estaria bueno reutilizar esto para disponibilizarlo
*/

with mov as (
                select
                	tx,
                	metadata,
                	metadata ->> 'amount' as amount,
                	elem->>'movement' as mov
                from ledger.movement
                cross join json_array_elements(metadata::json->'subTrxs') elem
                where details ilike 'Cobraste por link de pago%'
                )
                select
                	tx,
                	metadata ->> 'source' as external_ref,
                	ROUND(amount::DECIMAL /100,2) as amount,
                	mov::json -> 'inserted' -> 'metadata' -> 'request' ->> 'card_number' as card_number,
                	mov::json -> 'inserted' -> 'metadata' -> 'request' -> 'customer' ->> 'email' as email_payer,
                	mov::json -> 'inserted' -> 'metadata' -> 'request' -> 'customer' ->> 'ip_address' as ip_address_payer,
                	metadata
                from mov
                
-- https://stackoverflow.com/questions/45743778/postgresql-json-array-query

