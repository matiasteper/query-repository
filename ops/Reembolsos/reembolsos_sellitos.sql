/*
Consulta para reembolsos por "Sellitos".

************INSTRUCCIONES************
Modificar el valor entre "SELECT" y "as fecha_consulta" por la fecha del día que se quiere reembolsar (current_date -1 para reembolsar tx de día anterior).

Consideraciones:
- Sólo útil para 1 sólo día de consulta.
- Se tienen en cuenta tx entre 20211203 y la fecha de consulta.
- Se toman para reembolsar sólo tx de fecha de consulta.
- 
*/

WITH 
	parametro_consulta	as
	(
		SELECT date('2022-01-10') as fecha_consulta
	),
	
	tx_usr as
	(
		SELECT 
			o.external_ref, 
			parametro_consulta.fecha_consulta,
			count(distinct t.transaction_id) as cantidad_tx_usr,
			count(distinct case when date(m.created_at) = parametro_consulta.fecha_consulta then t.transaction_id else null end) as cantidad_tx_dia_consulta,
			max(m.created_at) as ult_tx
		FROM ledger.transaction t
		LEFT JOIN ledger.movement m ON t.movement_id = m.id
		LEFT JOIN ledger.origin  o ON t.origin_id = o.origin_id
		LEFT JOIN app.account ac ON o.external_ref = ac.account_hash
		,parametro_consulta
		WHERE 
			t.transaction_type_id IN (1,20,21,22,29,30,32,33) 
			AND date(m.created_at) BETWEEN '2022-01-01' AND parametro_consulta.fecha_consulta --tx desde validez de promo a fecha
			AND abs(m.amount) >= 400 --tx de 400$ o más
			AND ac.created_at - '03:00:00'::time without time zone::interval >= '2022-01-01' --Se creó la cuenta desde el 01/01/2022
		GROUP BY o.external_ref
			,parametro_consulta.fecha_consulta
		HAVING 
			count(distinct t.transaction_id) > 1 --se valida que haya hecho al menos 2 tx para hacer el reembolso
			AND count(distinct case when date(m.created_at) = parametro_consulta.fecha_consulta then t.transaction_id else null end) > 0 --al menos una tx en día de consulta (para reembolso)
	)		

SELECT 
	date(m.created_at) AS transaction_date, 
	ac.phone,
	o.external_ref AS extref_origin, 
	'Bonificacion Sellitos' AS message,
	case
		when cantidad_tx_usr = 2 and cantidad_tx_dia_consulta = 1 then 1
		when cantidad_tx_usr = 2 and cantidad_tx_dia_consulta = 2 then 1
		when cantidad_tx_usr = 3 and cantidad_tx_dia_consulta = 1 then 1
		when cantidad_tx_usr = 3 and cantidad_tx_dia_consulta = 2 then 2
		when cantidad_tx_usr = 3 and cantidad_tx_dia_consulta = 3 then 2
		when cantidad_tx_usr > 3 and cantidad_tx_usr - cantidad_tx_dia_consulta = 0 then 2
		when cantidad_tx_usr > 3 and cantidad_tx_usr - cantidad_tx_dia_consulta = 1 then 2
		when cantidad_tx_usr > 3 and cantidad_tx_usr - cantidad_tx_dia_consulta = 2 then 1
		when cantidad_tx_usr > 3 and cantidad_tx_usr - cantidad_tx_dia_consulta > 2 then 0
	end as cantidad_reembolsos
FROM ledger.movement m
LEFT JOIN ledger.transaction t ON m.id = t.movement_id
LEFT JOIN ledger.origin o ON t.origin_id = o.origin_id
INNER JOIN tx_usr ON o.external_ref = tx_usr.external_ref
LEFT JOIN app.account ac ON o.external_ref = ac.account_hash
,parametro_consulta
WHERE t.transaction_type_id IN (1,20,21,22,29,30,32,33)
	AND abs(m.amount) >= 400
	AND date(m.created_at) = parametro_consulta.fecha_consulta
GROUP BY 1,2,3,4,5
ORDER BY transaction_date