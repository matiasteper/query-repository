WITH first AS (
SELECT     external_ref,
        min(m1.created_at) AS first_transaction
FROM tap_l1_pg_ledger_public.movement m1
LEFT JOIN tap_l1_pg_ledger_public.transaction t1 ON m1.id = t1.movement_id
LEFT JOIN tap_l1_pg_ledger_public.origin o1 ON t1.origin_id = o1.origin_id
WHERE transaction_type_id = 5
    AND json_extract_path_text(m1.metadata, 'is_qr') = 'true'
    AND m1.amount < 0
    AND private = false
GROUP BY external_ref
   )

   SELECT
   m1.created_at AS transaction_date,
   t1.transaction_id AS transaction,
   a1.phone ,
   o1.external_ref AS extref_origin,
   CASE WHEN 0.5*abs(m1.amount) <= 500 THEN 0.5*abs(m1.amount)
   ELSE 500
   END AS amount,
   'Bonificacion Pago QR' AS message
FROM tap_l1_pg_ledger_public.movement m1
LEFT JOIN tap_l1_pg_ledger_public.transaction t1
   ON m1.id = t1.movement_id
LEFT JOIN tap_l1_pg_ledger_public.origin o1
   ON t1.origin_id = o1.origin_id
LEFT JOIN tap_l1_pg_app_public.account a1
   ON o1.external_ref = a1.account_hash
LEFT JOIN first f
   ON o1.external_ref = f.external_ref
WHERE
       f.first_transaction = m1.created_at
        AND date(m1.created_at) = '2022-01-23'
       AND transaction_type_id = 5
       AND json_extract_path_text(m1.metadata, 'is_qr') = 'true'
       AND m1.amount < 0
        AND m1.private = false
GROUP BY transaction_date, transaction, phone, extref_origin, m1.amount