WITH primera_recarga AS (
	   	  SELECT o.external_ref, Min(date(m.created_at)) as primera_recarga_mide
		  FROM      ledger.movement m
          LEFT JOIN ledger.TRANSACTION t
          ON        m.id = t.movement_id
          LEFT JOIN ledger.origin o
          ON        t.origin_id = o.origin_id
          WHERE     o.external_ref IN ('15d16c7c-eb82-4920-8bcf-a9f36916d52e',
									   '25643327-0300-4698-a887-bb24f6e7f04e')
          AND       t.transaction_type_id = 29
          GROUP BY  o.external_ref
		  HAVING Min(date(m.created_at)) = date(CURRENT_DATE -1)),


medidores_referidos AS
(
	
		  
		  SELECT    Min(m.created_at) as primera_recarga,
                    o.external_ref,
                    Split_part(Split_part(metadata -> 'ticket' ->> 'Ticket','Medidor Nro. ',2),'"',1) AS medidor
          FROM      ledger.movement m
          LEFT JOIN ledger.TRANSACTION t
          ON        m.id = t.movement_id
          LEFT JOIN ledger.origin o
          ON        t.origin_id = o.origin_id
          WHERE     o.external_ref IN (select external_ref from primera_recarga)
          AND       t.transaction_type_id = 29
          GROUP BY  o.external_ref,
                    medidor
 
			),

cantidad_usuarios as (				
			
SELECT    m.created_at,
          o.external_ref,
          Split_part(Split_part(metadata -> 'ticket' ->> 'Ticket','Medidor Nro. ',2),'"',1) AS medidor_mide
FROM      ledger.movement m
LEFT JOIN ledger.TRANSACTION t
ON        m.id = t.movement_id
LEFT JOIN ledger.origin o
ON        t.origin_id = o.origin_id

WHERE      t.transaction_type_id = 29
AND       date(m.created_at)<=CURRENT_DATE -1
AND       Split_part(Split_part(metadata -> 'ticket' ->> 'Ticket','Medidor Nro. ',2),'"',1) IN
          (
                 SELECT medidor
                 FROM   medidores_referidos)
				),
								 
medidores AS(

         SELECT   medidor_mide,
                  count (DISTINCT external_ref)
         FROM     cantidad_usuarios
         GROUP BY medidor_mide
         HAVING   count (DISTINCT external_ref) <= 3 )
		 
SELECT external_ref
FROM   medidores_referidos
WHERE  medidor IN
       (
              SELECT medidor_mide
              FROM   medidores)
			  
			  
			  
		