--Query 1 - 25 usuarios nuevos con 1 solo servicio pago --

SELECT
person.name,
last_name,
account.email,
account_hash,
document,
account.created_at,
COUNT(DISTINCT transaction.transaction_id) AS cant_tx
FROM tap_l1_pg_ledger_public.transaction AS transaction
INNER JOIN tap_l1_pg_ledger_public.origin AS origin ON transaction.origin_id = origin.origin_id
INNER JOIN tap_l1_pg_app_public.account AS account ON origin.external_ref = account.account_hash
INNER JOIN tap_l1_pg_app_public.person AS person ON account.account_id = person.account_id
WHERE (account.created_at BETWEEN '2022-01-01' AND '2022-01-31'
and transaction.transaction_type_id = 1)
GROUP BY 	
person.name,
last_name,
account.email,
account_hash,
document,
account.created_at
HAVING cant_tx = 1
LIMIT 25;


--Query 2 - 25 usuarios nuevos con 3 servicios pagos en los ultimos 3 meses y pdv -- 

SELECT
person.name,
last_name,
account.email,
account_hash,
lifetest.document,
account.created_at,
COUNT(DISTINCT transaction.transaction_id) AS cant_tx
FROM tap_l1_pg_ledger_public.transaction AS transaction
INNER JOIN tap_l1_pg_ledger_public.origin AS origin ON transaction.origin_id = origin.origin_id
INNER JOIN tap_l1_pg_app_public.account AS account ON origin.external_ref = account.account_hash
INNER JOIN tap_l1_pg_app_public.person AS person ON account.account_id = person.account_id
INNER JOIN tap_l1_pg_lifetest_public.life_tests_completed AS lifetest ON account.account_hash = lifetest.external_ref
WHERE (lifetest.created_at <  transaction.created_at
  AND transaction.created_at  BETWEEN '2021-11-01' AND '2022-01-31'
  AND transaction.transaction_type_id = 1)
GROUP BY 	
person.name,
last_name,
account.email,
account_hash,
lifetest.document,
account.created_at
HAVING cant_tx >= 3
LIMIT 25;



--Mismo que antes pero con tres servicios distintos -- 

SELECT
person.name,
last_name,
account.email,
account_hash,
lifetest.document,
account.created_at,
COUNT(DISTINCT movement.target) AS cant_targets
FROM tap_l1_pg_ledger_public.movement AS movement
LEFT JOIN tap_l1_pg_ledger_public.transaction AS transaction ON movement.id = transaction.id 
INNER JOIN tap_l1_pg_ledger_public.origin AS origin ON transaction.origin_id = origin.origin_id
INNER JOIN tap_l1_pg_app_public.account AS account ON origin.external_ref = account.account_hash
INNER JOIN tap_l1_pg_app_public.person AS person ON account.account_id = person.account_id
INNER JOIN tap_l1_pg_lifetest_public.life_tests_completed AS lifetest ON account.account_hash = lifetest.external_ref
WHERE (lifetest.created_at <  transaction.created_at
  AND transaction.created_at  BETWEEN '2021-11-01' AND '2022-01-31'
  AND transaction.transaction_type_id = 1)
GROUP BY 	
person.name,
last_name,
account.email,
account_hash,
lifetest.document,
account.created_at
HAVING cant_targets >= 3
LIMIT 25;
