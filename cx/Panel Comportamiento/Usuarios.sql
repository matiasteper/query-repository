WITH prepaid AS (
    SELECT external_ref,
           COUNT (pc1.*) OVER (PARTITION BY pc1.account_id) as cant_tarjetas
    FROM tap_l1_pg_prepaid_public.card pc1
    LEFT JOIN tap_l1_pg_prepaid_public.account pa1 ON pc1.account_id = pa1.id
    LEFT JOIN tap_l1_pg_prepaid_public.origin po1 ON pa1.origin_id = po1.id
    GROUP BY external_ref
    )

SELECT blacklist,
       name,
       balance,
       email,
       external_ref,
       dni,
        birthdate,
       da1.created_at as fecha_creacion_cuenta,
       natural_person,
       phone,
       cvu,
       life_test,
       device_id,
       version,
       document as cuit_cuil,
       device_os,
       province_id,
       exposed_person,
        CASE
            WHEN cant_tarjetas = 1 OR cant_tarjetas = 2
            THEN true
        ELSE false
        END AS tarjeta_virtual,
        CASE
            WHEN cant_tarjetas = 2
            THEN true
        ELSE false
        END AS tarjeta_fisica
FROM tap_l2.dim_account da1
LEFT JOIN prepaid USING (external_ref)
WHERE
        document = 'x'
        OR cvu = 'x'
        OR external_ref = 'x'
        OR name = 'x'
        OR phone = 'x'
        OR document = 'x'
        OR email = 'meliherszage@gmail.com'
        OR device_id = 'x'