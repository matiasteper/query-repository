 SELECT c.id AS card_id,
    c.account_id,
    c.created_at AS card_created_at,
    c.card_type,
    c.card_status_id,
    c.number AS card_number,
    c.reference,
    pt.id AS transaction_id,
    pt.external_ref AS external_ref_pt,
    pt.status,
    pt.type,
    pt.commerce_name,
    pt.reason_id AS reason_id_cancel,
    pt.created_at,
    o.external_ref AS external_ref_origin,
    a.provider_account_id
   FROM prepaid.card c
     LEFT JOIN prepaid.transaction pt ON c.id = pt.card_id
     LEFT JOIN prepaid.account a ON c.account_id = a.id
     LEFT JOIN prepaid.origin o ON a.origin_id = o.id;