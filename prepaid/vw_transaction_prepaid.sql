 WITH first AS (
         SELECT t.origin_id,
            min(m.created_at) AS first_transaction
           FROM ledger.transaction t
             LEFT JOIN ledger.movement m ON t.movement_id = m.id
          WHERE (t.transaction_type_id = ANY (ARRAY[1, 20, 21, 22, 30, 31, 32, 33, 34])) OR t.transaction_type_id = 5 AND m.amount < 0::numeric
          GROUP BY t.origin_id
        )
 SELECT c.id AS card_id,
    c.account_id,
    c.created_at AS card_created_at,
    c.card_type,
    c.card_status_id,
    c.number AS card_number,
        CASE
            WHEN m1.origin_type = 'MANUAL'::text THEN concat(m1.id, 'D-', m1.tx)
            ELSE t1.transaction_id
        END AS transaction_id,
        CASE
            WHEN tt.description = 'WORKER ACCOUNT BALANCER'::text AND lower(m1.details) ~~ '%a tu cvu%'::text THEN 'EXTERNAL TRANSFER'::text
            WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND lower(m1.details) ~~ '%a tu cvu%'::text THEN 'EXTERNAL TRANSFER'::text
            WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND lower(m1.details) ~~ '%ajuste%'::text THEN 'WORKER ACCOUNT BALANCER'::text
            WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND lower(m1.details) ~~ '%transferencia manual%'::text THEN 'TRANSFER TAP TO TAP'::text
            WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND lower(m1.details) ~~ '%te transfi%'::text THEN 'TRANSFER TAP TO TAP'::text
            WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND lower(m1.details) ~~ '%fue transfe%'::text THEN 'TRANSFER TAP TO TAP'::text
            WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND lower(m1.details) ~~ '%acreditado desde la tarjeta%'::text AND m1.details ~~ '%bito%'::text THEN 'CASH IN DEBIT CARD'::text
            WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND lower(m1.details) ~~ '%acreditado desde la tarjeta%'::text AND m1.details !~~ '%bito%'::text THEN 'CASH IN CREDIT CARD'::text
            WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND lower(m1.details) ~~ '%transferiste a%'::text THEN 'TRANSFERS TO CBU'::text
            WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND lower(m1.details) ~~ '%cash in a la wc%'::text THEN 'WC CASH IN'::text
            WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND lower(m1.details) ~~ '%la cuenta wc%'::text AND lower(m1.details) ~~ '%fondeo%'::text THEN 'WC_FOUND'::text
            WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND lower(m1.details) ~~ '%worker concilia%'::text THEN 'WORKER ACCOUNT BALANCER'::text
            WHEN lower(m1.details) ~~ '%bonifica%'::text OR lower(m1.details) ~~ '%reintegro%'::text OR lower(m1.details) ~~ '%devoluc%'::text OR lower(m1.details) ~~ '%reembols%'::text THEN 'BONIF/REINTEGROS'::text
            ELSE tt.description
        END AS description,
    t1.transaction_type_id,
    to_char(m1.created_at - '00:00:00'::time without time zone::interval, 'HH24:MI:SS'::text) AS transaction_hour,
    to_char(m1.created_at - '00:00:00'::time without time zone::interval, 'YYYY-MM-DD'::text) AS transaction_day,
    m1.id AS movement_id,
    m1.created_at - '00:00:00'::time without time zone::interval AS transaction_date,
    m1.amount,
    m1.origin_id,
    m1.target,
    m1.private,
    m1.metadata ->> 'isInternational'::text AS isinternational,
    o1.external_ref,
    o1.name,
    a1.email,
    a1.phone,
    a1.created_at - '00:00:00'::time without time zone::interval AS account_creation_date,
    f1.first_transaction - '00:00:00'::time without time zone::interval AS first_transaction,
    pt.external_ref AS external_ref_pt,
    pt.status,
    pt.type,
    pt.commerce_name,
    pt.reason_id AS reason_id_cancel,
    pa.provider_account_id,
    to_char(f1.first_transaction - '00:00:00'::time without time zone::interval, 'HH24:MI:SS'::text) AS first_transaction_hour,
    to_char(f1.first_transaction - '00:00:00'::time without time zone::interval, 'YYYY-MM-DD'::text) AS first_transaction_day,
    date_part('year'::text, age(CURRENT_DATE::timestamp with time zone, ap1.birthdate::timestamp with time zone)) AS edad
FROM ledger.movement m1
     LEFT JOIN ledger.transaction t1 ON t1.movement_id = m1.id
     LEFT JOIN ledger.transaction_type tt ON t1.transaction_type_id = tt.transaction_type_id
     LEFT JOIN prepaid.transaction pt ON pt.external_request_id = (m1.metadata ->> 'externalRequestId'::text)
     LEFT JOIN ledger.origin o1 ON m1.origin_id::integer = o1.origin_id
	 LEFT JOIN prepaid.card c ON c.id = pt.card_id
     LEFT JOIN prepaid.account pa ON c.account_id = pa.id
     LEFT JOIN app.account a1 ON o1.external_ref = a1.account_hash
     LEFT JOIN app.person ap1 ON a1.account_id = ap1.account_id
     LEFT JOIN first f1 ON m1.origin_id::integer = f1.origin_id
  WHERE date(m1.created_at) > '2020-12-31'::date