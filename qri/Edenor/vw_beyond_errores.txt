SELECT 
 	o1.created_at as transaction_date,
    o1.tx,
    o1.external_ref,
    o1.response ->> 'code'::text AS error_code,
	o1.response ->> 'name'::text AS error_name,
	o1.response ->> 'message'::text AS error_message,
    o1.status,
	p1.payment_id,
	p1.transaction_id
   FROM beyond.operations o1 
	LEFT JOIN beyond.payments p1 USING (operation_id)