# README #

## Repositorio colaborativo para analistas de Tap con el próposito de compartir análisis y cálculos en SQL ##



### Liniamientos para compartir consultas ###

* Escribir nombre del cálculo y tablas involucradas
* Documentar quién lo hizo y cuándo


### ¿Quién puede visualizar las consultas en este repo? ###

* Todxs en Tap


### ¿Quién puede crear nuevas consultas en este repo? ###

* Todxs en Tap
