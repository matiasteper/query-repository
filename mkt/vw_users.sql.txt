SELECT 
ac.account_hash AS external_ref,
ap.name AS nombre,
ap.last_name AS apellido,
ap.cuit AS cuit,
EXTRACT (YEAR FROM age(current_date, ap.birthdate) )AS edad,
CASE
     WHEN LEFT(ap.cuit,2) = '20' THEN 'Hombre'
     WHEN LEFT(ap.cuit,2) = '27' THEN 'Mujer'
	 ELSE 'N/A'
  END AS sexo,
ap.province AS ubicacion,
lo.balance AS saldo_en_cuenta,
CASE
    WHEN lo.origin_id IN (
    SELECT distinct origin_id
	FROM ledger.transaction as lt
	WHERE transaction_type_id IN ('20','21')
	) THEN '1'
    ELSE '0'
  END AS recarga,
CASE
    WHEN lo.origin_id IN (
    SELECT distinct origin_id
	FROM ledger.transaction as lt
	WHERE transaction_type_id IN ('1','22')
	) THEN '1'
    ELSE '0'
  END AS servicios,
CASE
    WHEN lo.origin_id IN (
    SELECT distinct origin_id
	FROM ledger.transaction as lt
	WHERE transaction_type_id IN ('29')
	) THEN '1'
    ELSE '0'
  END AS mide
FROM app.person AS ap
LEFT JOIN app.account AS ac on (ap.account_id = ac.account_id)
LEFT JOIN ledger.origin AS lo ON (ac.account_hash = lo.external_ref)